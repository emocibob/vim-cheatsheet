﻿# Vim šalabahter

## Pokretanje i izlaz

- `$ vim file.txt` - pokreni u terminalu i otvori `file.txt`
- `:q` - izađi
- `:wq` - spremi i izađi
- `:q!` - izađi bez spremanja izmjena

## Spremanje

- `:w` - spremi

## Uređivanje teksta

- `i` - uređivanje teksta na mjesto kursora
- `a` - uređivanje teksta na mjesto nakon kursora
- `o` - uređivanje u novom retku ispod trenutnog
- `u` - korak unazad (*undo*)
- `Ctrl`+`r` - korak unaprijed (*redo*)
- `yy` - kopiraj redak
- `3yy` - kopiraj 3 redaka
- `yw` - kopiraj riječ (počevši od pozicije kursora)
- `p` - zalijepi desno od kursora
- `P` - zalijepi lijevo od kursora
- `cw` - izbriši riječ (počevši od pozicije kursora) i kreni u uređivanje
- `dw` - izbriši riječ (počevši od pozicije kursora)
- `c$` - izbriši redak (počevši od pozicije kursora) i kreni u uređivanje
- `D` - izbriši redak (počevši od pozicije kursora)
- `dd` - izbriši redak
- `dG` - izbriši sav tekst od kursora do kraja datoteke

## Kretanje



## Upravljanje prozorima



## Razno

